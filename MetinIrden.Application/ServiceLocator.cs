﻿using MetinIrden.Business;

namespace MetinIrden.Application
{
    public class ServiceLocator
    {
        public ServiceLocator(UserService userService, ProductService productService)
        {
            UserService = userService;
            ProductService = productService;
        }

        public UserService UserService { get; private set; }

        public ProductService ProductService { get; private set; }
    }
}