﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;
using MetinIrden.Business;

namespace MetinIrden.Application.DesignTimeServices
{
    public class ServiceLocatorDesignTimeServices : IDesignTimeServices
    {
        public void ConfigureDesignTimeServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<UserService, UserService>();
            serviceCollection.AddScoped<ProductService, ProductService>();

            serviceCollection.AddScoped<ServiceLocator, ServiceLocator>();
        }
    }
}