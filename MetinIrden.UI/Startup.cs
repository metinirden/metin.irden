﻿using Exceptionless;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MetinIrden.Data;
using MetinIrden.Application.DesignTimeServices;
using MetinIrden.Util;

namespace MetinIrden.UI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //Logger Configuration
            services.AddScoped<AsyncLogger, AsyncLogger>();
            services.AddScoped<EmailSender, EmailSender>();

            //DbContext registration.
            services.AddEntityFrameworkInMemoryDatabase()
                .AddDbContext<MetinIrdenContext>()
                //    opt =>
                //{
                //    opt.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
                //}, ServiceLifetime.Scoped);
                .AddUnitOfWork<MetinIrdenContext>();

            //Application Services registration.
            new ServiceLocatorDesignTimeServices().ConfigureDesignTimeServices(services);

            //Cookie Policy
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //MVC
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // 3 Stage Environment
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else if (env.IsProduction() || env.IsStaging())
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseExceptionless("F6jiy7lM7UpQqWhksP3xllLiRZHZE4UzKGLEZRlz");

            // Gzip Support for Css Js
            app.UseStaticFiles(new StaticFileOptions()
            {
                OnPrepareResponse = (context) =>
                {
                    if ("application/x-gzip".Equals(context.Context.Response.Headers["Content-Type"]))
                    {
                        if (context.File.Name.EndsWith("js.gz"))
                            context.Context.Response.Headers["Content-Type"] = "application/javascript";
                        else if (context.File.Name.EndsWith("css.gz"))
                            context.Context.Response.Headers["Content-Type"] = "text/css";

                        context.Context.Response.Headers.Add("Content-Encoding", "gzip");
                    }
                    context.Context.Response.Headers.Add("Cache-Control", "public, max-age=2592000");
                }
            });

            app.UseCookiePolicy();
            app.UseMvcWithDefaultRoute();
        }
    }
}
