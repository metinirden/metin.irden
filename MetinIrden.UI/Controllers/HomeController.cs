﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MetinIrden.Application;
using MetinIrden.UI.Models;
using MetinIrden.Util;

namespace MetinIrden.UI.Controllers
{
    public class HomeController : CustomController
    {
        public HomeController(ServiceLocator serviceLocator, AsyncLogger logger) : base(serviceLocator, logger) { }

        public IActionResult Index()
        {
            return View();
            
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

    public class CustomController : Controller
    {
        protected ServiceLocator ServiceLocator { get; private set; }
        protected AsyncLogger Logger { get; private set; }

        public CustomController(ServiceLocator serviceLocator, AsyncLogger logger)
        {
            Logger = logger;
            ServiceLocator = serviceLocator;
        }
    }
}
