﻿using System;
using System.Threading.Tasks;
using Exceptionless;
using Exceptionless.Models.Data;
using Microsoft.Extensions.Logging;

namespace MetinIrden.Util
{
    public class AsyncLogger
    {
        public void Log(LogLevel level, Exception ex, string message = null, bool isCritical = false, UserInfo userInfo = null, params object[] extraObjs)
        {
            Task.Run(() =>
            {
                EventBuilder logEvent = ex
                    .ToExceptionless()
                    .SetReferenceId(Guid.NewGuid().ToString("N"));
                if (isCritical)
                    logEvent.MarkAsCritical();
                if (extraObjs != null)
                    foreach (object obj in extraObjs)
                        logEvent.AddObject(obj, obj.GetType().Name, maxDepth: 3);
                if (message != null)
                    logEvent.SetMessage(message);
                if (userInfo != null)
                    logEvent.SetUserIdentity(userInfo);
                logEvent.Submit();
            });
        }
    }
}