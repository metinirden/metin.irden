﻿using System;
using System.Linq;
using FluentEmail.Core;
using FluentEmail.Core.Models;
using Microsoft.Extensions.Logging;

namespace MetinIrden.Util
{
    public class EmailSender
    {
        private readonly AsyncLogger _logger;

        public EmailSender(AsyncLogger logger)
        {
            _logger = logger;
        }

        public void SendEmail(string from, string[] toList, string subject, string body, string[] ccList, string[] bccList)
        {
            IFluentEmail email = Email.From(from)
                .To(toList.Select(i => new Address(i)).ToArray())
                .CC(ccList.Select(i => new Address(i)).ToArray())
                .BCC(bccList.Select(i => new Address(i)).ToArray())
                .Subject(subject)
                .Body(body);
            try
            {
                email.SendAsync();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Warning, ex, "Mail gönderilemedi", true, null, email.Data);
            }
        }
    }
}