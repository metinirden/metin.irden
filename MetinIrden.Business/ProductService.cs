﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using MetinIrden.Business.Base;

namespace MetinIrden.Business
{
    public class ProductService : Service
    {
        public ProductService(IUnitOfWork unitOfWork, IMemoryCache memoryCache) : base(unitOfWork, memoryCache) { }
    }
}