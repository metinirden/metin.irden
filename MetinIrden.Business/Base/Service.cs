﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace MetinIrden.Business.Base
{
    public abstract class Service : IDisposable
    {
        protected readonly IUnitOfWork UnitOfWork;
        protected readonly IMemoryCache MemoryCache;

        protected Service(IUnitOfWork unitOfWork, IMemoryCache memoryCache)
        {
            UnitOfWork = unitOfWork;
            MemoryCache = memoryCache;
        }

        public void Dispose() => UnitOfWork?.Dispose();
    }
}
