﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using MetinIrden.Business.Base;
using MetinIrden.Data.Entities;

namespace MetinIrden.Business
{
    public class UserService : Service
    {
        public UserService(IUnitOfWork unitOfWork, IMemoryCache memoryCache) : base(unitOfWork, memoryCache) { }

    }
}