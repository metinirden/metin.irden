﻿using Microsoft.EntityFrameworkCore;
using MetinIrden.Data.Entities;
using MetinIrden.Data.Mappings;

namespace MetinIrden.Data
{
    public class MetinIrdenContext : DbContext
    {
        public MetinIrdenContext(DbContextOptions options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new ProductMap());
            modelBuilder.EnableAutoHistory(null);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseInMemoryDatabase("test");
        }
    }
}
