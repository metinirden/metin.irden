﻿using System;

namespace MetinIrden.Data.Base
{
    public interface ICreateAudit<T>
    {
        DateTime CreateDate { get; set; }
        T CreatedBy { get; set; }
    }

    public interface IUpdateAudit<T>
    {
        DateTime UpdateDate { get; set; }
        T UpdatedBy { get; set; }
    }
}
