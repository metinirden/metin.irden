﻿using System;

namespace MetinIrden.Data.Base
{
    public class Entity<T>
    {
        public T ID { get; set; }
    }

    public class Entity : Entity<int> { }

    public class FullyAuditEntity<T> : Entity, ICreateAudit<T>, IUpdateAudit<T>
    {
        public DateTime CreateDate { get; set; }
        public T CreatedBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public T UpdatedBy { get; set; }
    }
}
